build_lib(
  LIBNAME epidemic-routing
  SOURCE_FILES
    helper/epidemic-helper.cc
    model/epidemic-packet.cc
    model/epidemic-packet-queue.cc
    model/epidemic-routing-protocol.cc
    model/epidemic-tag.cc
  HEADER_FILES
    helper/epidemic-helper.h
    model/epidemic-packet.h
    model/epidemic-packet-queue.h
    model/epidemic-routing-protocol.h
    model/epidemic-tag.h
  LIBRARIES_TO_LINK ${libinternet}
  TEST_SOURCES
    test/epidemic-test-suite.cc
)
